# layui-tableTreeDj
## 先来个图片
![效果图](./demo.jpg)

## 后端数据返回示例
> 不需要children, 维护好id与pid的关系即可.id与pid的名称支持修改

```json
{
  "code": 0,
  "msg": "ok",
  "count": 100,
  "data": [
    {"id": 1, "title": "a", "pid":  0},
    {"id": 2, "title": "b", "pid":  1},
    {"id": 3, "title": "c0", "pid":  2},
    {"id": 4, "title": "c1", "pid":  2},
    {"id": 5, "title": "c2", "pid":  2}
  ]
}
```

#### 文档链接
 [首页](./README.md)  [简单版本](./a.md) [懒加载版](./b.md)
> 如果数据不是成千上万,简单版本就完全够用的

## 介绍
使表格增加了树形结构展示的能力.完全依赖于layui的表格.
正因为如此,您可以像使用表格组件一样使用该组件.layui的表格功能全都有.全都有.全都有.

## 常见问题汇总:
#### 1. 升级到layui 2.6.11版本 “折叠图标列显示异常 ” layui 2.6.10版本及以下可以
```
原因是table默认开启了escape：true
注：从 v2.6.11 开始，默认开启。
所以在渲染表格时，escape: false 可以解决
此配置项为true时会破坏html标签
```
