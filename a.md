## 简单版本

#### 文档链接
[首页](./README.md)  [简单版本](./a.md) [懒加载版](./b.md)

#### 代码示例
```javascript

layui.use(['tableTreeDj'], function() {
        const tableTree = layui.tableTreeDj;
        const $ = layui.$;

        // 与 layui.table 的参数完全一致,内部本来就是把这些参数传递给table模块的
        const objTable = {
            elem: '#test'
            ,url: "./getData"
            ,cols: [[
                {field:'name', title:'名称' },
                {field:'id', title:'ID' },
                {field:'pid', title:'上级ID' },
                {field:'level', title:'层级' },
                {field:'agent_id', title:'代理ID' },
            ]]
            ,id:'list'
        }

        // 本组件用到的参数, 组件内部有默认值,与此一致,因此您可以只声明不一致的配置项
        const config = {
            keyId: "id" // 当前ID
            , keyPid: "pid" // 上级ID
            , title: "name" // 标题名称字段,此字段td用于绑定单击折叠展开功能
            , indent: ' &nbsp; &nbsp;' // 子级td的缩进.可以是其他字符
            // 图标
            , icon: {
                open: 'layui-icon layui-icon-triangle-d', // 展开时候图标
                close: 'layui-icon layui-icon-triangle-r', // 折叠时候图标
            }
            , showCache: true //是否开启展开折叠缓存,默认不开启.
        };
        // 渲染 
        tableTree.render(objTable, config);

        // 其他一系列操作后.重新渲染表格,此时可以不传递参数.内部记录了上次传入的参数.
        tableTree.render();
        
        // 点击搜索按钮后重载数据.此时可以传入where条件.obj参数与官方表格一致.
        obj = {where:{id: 1}};
        tableTree.reload(obj);


    });
```

注意:
1. 本组件仅支持url方式获取数据.见谅.可以将数据放入另一个json文件.然后用url方式引入(url:'./data.json')
2. 仓库有个ie分支.没有使用es6的语法.兼容ie浏览器.
3. 需要一次性查询出全部数据.至少上下级必须完整

#### 组件引入方法请阅读 官方文档
https://www.layui.com/doc/base/modules.html#extend

#### 方法
* render(): 表格渲染.一般第一次显示调用.或者其他操作比如删除/添加等操作后也可以调用.第二次调用可以不传参数.如果传递参数会将上次参数覆盖.
* reload(): 表格重载,内部调用了table.reload().一般用于搜索后显示数据.提交where条件给后端.
* getTable(): 由于该组件内部使用了layui.table.如果想更细粒度的操作table.可以使用此方法获取table对象
* 其他方法: 请阅读源码,只要方法名不以下划线开头都可以使用.如果需要的话.


#### 参数

|  参数名称   | 必填  | 默认值 | 含义                                                                                |
|  ----  | ----  | ---- |-----------------------------------------------------------------------------------|
| keyId  | 否 | id | 数据主键                                                                              |
| keyPid  | 否 | pid | 父级ID字段名称                                                                          |
| title  | 否 | name | 用于点击触发展开折叠的字段                                                                     |
| indent  | 否 | ` &nbsp; &nbsp; ` | 缩进字符                                                                              |
| icon.open  | 否 | layui-icon layui-icon-triangle-d | 展开时候的图标                                                                           |
| icon.close  | 否 | layui-icon layui-icon-triangle-r | 折叠时候的图标                                                                           |
| showCache  | 否 | false | 折叠状态是否缓存,可传字符串.表示缓存的key.缓存记录的位置为 `localStorage`                                   |
| sort  | 否 | asc | 排序方式,只支持小写                                                                        |
| defaultShow  | 否 | false | 是否默认全部展开,优先级低于 `showCache` 字段                                                     |
| titleUseTemplet  | 否 | false | 标题使用templet方法,此时不会主动给标题添加图标与缩进.如果需要可以在templet中调用 setTitleIcon 与 setTitleIndent 方法 |

