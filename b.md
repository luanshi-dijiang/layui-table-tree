## 懒加载版

#### 文档链接
[首页](./README.md)  [简单版本](./a.md) [懒加载版](./b.md)

#### 代码示例
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="../../../layui/src/css/layui.css" media="all">
    <script type="text/javascript" src="../../../layui/src/layui.js"></script>
</head>
<body>

<script type="text/html" id="toolbarDemo">
    <div class="layui-btn-container">
        <button class="layui-btn layui-btn-sm" lay-event="add">添加</button>
        <button class="layui-btn layui-btn-sm" lay-event="delete">删除</button>
        <button class="layui-btn layui-btn-sm" lay-event="update">编辑</button>
    </div>
</script>

<table style="display: none" lay-filter="demo" id="demo">
    <thead>
    <tr>
        <th lay-data="{field:'id', width:100}">ID</th>
        <th lay-data="{field:'pid', width:100}">PID</th>
        <th lay-data="{field:'username', width:500}">昵称</th>
        <th lay-data="{field:'age', width:80, sort:true}">年龄</th>
        <th lay-data="{field:'sign'}">签名</th>
        <th lay-data="{toolbar: '#toolbarDemo'}">操作</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
<div id="page"></div>


<script type="text/javascript">
    layui.config({
        base: '../../../layui_exts/tableTreeDj/'
    });
    layui.use(['table','tableTreeRemoteDj'], function() {
        const table = layui.table
        const tableTree = layui.tableTreeRemoteDj;
        const $ = layui.$;
        const tableKey = 'demo'

        // 表格配置
        const objTable = {
            height:500
            , limit: 10000
        }

        // 组件配置
        const objTree = {
            tableKey: 'demo'
            ,fieldId: 'id'
            ,fieldClick: 'username'
            ,reqUrl: './getData.php'
            ,reqKey: 'pid'
            ,reqVal: 0
        }
        tableTree.render(objTable, objTree)
    });
</script>

</body>
</html>
```

#### 注意
1. 只支持 `自动渲染方式`.
2. 支持懒加载.在点击展开时候通过ajax获取数据.折叠时候.为了保证浏览器速度,删除折叠的数据.

#### 配置参数

|  参数名称   | 必填  | 默认值 | 含义 |
|  ----  | ----  | ---- | ---- |
| tabkeKey  | 否 | demo | 表格 `lay-filter` 属性 |
| fieldId  | 否 | id | ID字段的名称 |
| fieldClick  | 否 | username | 用于点击触发展开折叠的名称 |
| reqUrl  | 是 |  | 请求的url地址,其他参数可拼接到url中 |
| reqKey  | 否 | pid | 请求的key |
| reqVal  | 否 | 0 | 请求的值 |
| indent  | 否 | ` &nbsp; &nbsp; &nbsp;` | 缩进 |
| icon.open | 否 | layui-icon layui-icon-triangle-d | 展开时候的图标 |
| icon.close | 否 | layui-icon layui-icon-triangle-d | 折叠时候的图标 |

